package cn.mzcode.faker.webmagic.utils;

/**
 * Stands for features unstable.
 *
 * @author code4crafter@gmail.com <br>
 */
public @interface Experimental {
}
