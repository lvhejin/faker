package cn.mzcode.faker.webmagic.configurable;

/**
 * @author code4crafter@gmail.com
 */
public enum ExpressionType {

    XPath, Regex, Css, JsonPath;

}
