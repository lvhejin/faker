package cn.mzcode.faker.webmagic.monitor;

import org.junit.Test;
import cn.mzcode.faker.webmagic.Page;
import cn.mzcode.faker.webmagic.Site;
import cn.mzcode.faker.webmagic.Spider;
import cn.mzcode.faker.webmagic.processor.PageProcessor;

import javax.management.JMException;

/**
 * @author jerry_shenchao@163.com
 */
public class SeedUrlWithPortTest {

    @Test
    public void testSeedUrlWithPort() throws JMException {
        Spider spider = Spider.create(new TempProcessor()).addUrl("http://www.hndpf.org:8889/");
        SpiderMonitor.instance().register(spider);
        spider.run();
    }
}

class TempProcessor implements PageProcessor {

    @Override
    public void process(Page page) {

    }

    @Override
    public Site getSite() {
        return Site.me();
    }
}
